# Simple Reliable Serial Communication protocol (SRSC)

## Table of contents

- [Simple Reliable Serial Communication protocol (SRSC)](#simple-reliable-serial-communication-protocol-srsc)
  - [Table of contents](#table-of-contents)
  - [1 Introduction](#1-introduction)
  - [2 Principle](#2-principle)
  - [3 Packet structure](#3-packet-structure)
    - [3.1 Packet type](#31-packet-type)
      - [3.1.1 Packet type severity](#311-packet-type-severity)
      - [3.1.2 Packet type ranges](#312-packet-type-ranges)
    - [3.2 Checksum](#32-checksum)
      - [3.2.1 Calculation process](#321-calculation-process)
    - [3.3 Packet ID](#33-packet-id)
    - [3.4 Payload](#34-payload)
      - [3.4.1 Payload example](#341-payload-example)
  - [4 Reserved packet types](#4-reserved-packet-types)
      - [4.1 CONNECT](#41-connect)
        - [4.1.1 Packet structure](#411-packet-structure)
      - [4.2 CONNACK](#42-connack)
        - [4.2.1 Packet structure](#421-packet-structure)
      - [4.3 ACCEPTACK](#43-acceptack)
        - [4.3.1 Packet structure](#431-packet-structure)
      - [4.4 CRITACK](#44-critack)
        - [4.4.1 Packet structure](#441-packet-structure)
      - [4.5 REGTYPE](#45-regtype)
        - [4.5.1 Packet structure](#451-packet-structure)
        - [4.5.2 Payload structure](#452-payload-structure)
      - [4.6 REGACK](#46-regack)
        - [4.6.1 Packet structure](#461-packet-structure)
      - [4.7 Type 0](#47-type-0)
      - [4.8 Type 255](#48-type-255)
  - [5 Communication process flow](#5-communication-process-flow)
    - [5.1 Actors](#51-actors)
    - [5.2 Process](#52-process)
  - [6 Common API](#6-common-api)
    - [6.1 Data types](#61-data-types)
    - [6.2 Public API](#62-public-api)
      - [6.2.1 Methods](#621-methods)
    - [6.3 Optional data structures](#63-optional-data-structures)
      - [6.3.1 Severity](#631-severity)
      - [6.3.2 PacketType](#632-packettype)
        - [6.3.2.1 Methods](#6321-methods)
      - [6.3.3 Packet](#633-packet)
        - [6.3.3.1 Methods](#6331-methods)
      - [6.3.4 DataPacket](#634-datapacket)
        - [6.3.4.1 Methods](#6341-methods)
  - [7 Implementation notes](#7-implementation-notes)
    - [7.1 Connection](#71-connection)
    - [7.2 Sending packets](#72-sending-packets)
    - [7.3 Receiving packets](#73-receiving-packets)
    - [7.4 Critical packets](#74-critical-packets)
    - [7.5 Repeated packets](#75-repeated-packets)
    - [7.6 ID](#76-id)
    - [7.7 Packet type registration](#77-packet-type-registration)
  - [8 Current implementations](#8-current-implementations)
  - [9 Contribution](#9-contribution)
  - [10 License](#10-license)

## 1 Introduction

SRSC is a simple communication protocol that is built on top of serial communication and extends it with data integrity checking, message format definition and buffer overflow protection.

It is inspired by [Robust Arduino Serial protocol](https://github.com/araffin/arduino-robust-serial).

## 2 Principle

SRSC starts communication with simple connection. It tels both actors that the other one is listening and what is the size of opposite ones serial buffer. When the connection is established, both actors can start sending packets. Those have defined types that determine which action should be performed on the receiver side and what data is transferred. Structure and types of packets are described in chapter *3* and *4*.

Several error states can occur during communication - these are addressed by the protocol. The first of them is data corruption or complete loss of packet content. Each packet therefore contains a checksum that allows the receiver to determine the validity of the packet. If the packet is corrupted, the receiver discards it. This principle implies a potential problem for the protocol, which is described at the end of this chapter.

The second problem is buffer overflow. Some platforms have a very limited buffer for storing serial communication messages (e.g. standard Arduino has 64 bytes). Therefore, SRSC implements a semaphore and packet delivery acknowledgement method to guarantee that if the buffer is full on the receiver side, no data will be sent from the sender until the buffer is freed. The semaphore size is same as the buffer size. The principle of semaphore operation is as follows: when the sender sends a packet, the semaphore value is decreased according to the size of the packet (on the sender side), then when the ACCEPTACK packet with the size of the original packet is received (receiver sends it as answer to successfully accepted packet), it is increased again. In case the semaphore value is less than the size of sending packet, the sending process is suspended until the value is increased again.

The protocol does not implement resending of corrupted standard packets. If an application requires sending packets where delivery guarantee is critical, appropriate safeguards must be taken. One possibility is to use so-called critical packets, which are resent in case of failure. This method is suitable for more powerful devices. The second option is repeated packets, which are sent multiple times (3x). This method is not as reliable as critical packets but is more suitable for less powerful devices (e.g. microcontrollers).

## 3 Packet structure

| ***Byte*** | 0 | 1 | -/2 | 2/3 - EOP |
| ---------- | - | - | - | - |
| ***Information*** | Packet type | Checksum | Packet ID | Payload |

### 3.1 Packet type

- *size*: 1 byte
- *position*: 0

Packet type represents action or type of data in the packet payload. It also carries information about the criticality of the packet.

#### 3.1.1 Packet type severity

1. *Standard packets*
    - packets which have no guarantee of delivery
2. *Repeated packets*
    - packets sent 3 times
3. *Critical packets*
    - packets that are resent on failure 

#### 3.1.2 Packet type ranges

| First type | Last type | Range size | Description |
| ---------- | --------- | ---------- | ----------- |
| 0 | 0 | 1 | restricted |
| 1 | 15 | 15 | reserved for future versions of the protocol |
| 16 | 127 | 112 | user defined packets with no payload |
| 128 | 254 | 127 | user defined packets with payload |
| 255 | 255 | 1 | restricted (it is used by serial communication to indicate empty buffer) |

### 3.2 Checksum

- *size*: 1 byte
- *position*: 1

Sum used for error detection. It uses *1's Complement* algorithm.

It detects following kinds of error:

- `n` number bit flip
- complete data loss

#### 3.2.1 Calculation process

1. all bytes of the packet are summed together and anything exceeding 8 bits is discarded
2. the counted byte is then bitwise inverted and inserted to the packet
3. on the receiver side all bytes of the packet (including the checksum) are added together
4. the result is compared with the number 255 (`0b11111111`) - if they are equal, the packet arrived uncorrupted

[Error Masking Probability paper](http://newslab.cs.wayne.edu/changliICCCN2001.pdf)

### 3.3 Packet ID

- *size*: 1 byte
- *position*: 2 or none

Number used to identify critical packets or series of repeated packets (it is included only in critical and repeated packets). It's NOT totally unique identifier - it cycles over 256 values.

### 3.4 Payload

- *size*: >= 0
- *position*: 2/3 - packet end

Numerical information transmitted by the packet. Size of payload is determined by the first bit of each byte - `0` means the last byte of the payload (**[VLQ](https://en.wikipedia.org/wiki/Variable-length_quantity)** encoding). Bytes are ordered according to the Little-endian convention.

#### 3.4.1 Payload example

Payload value: **200**

| ***Byte*** | 0 | 1 |
| ---------- | - | - |
| ***Value*** | **1**0001000 | **0**0000011 |

- bold values are *last-byte indication bits*

## 4 Reserved packet types

#### 4.1 CONNECT

- *code*: `0x01`

An attempt to establish connection. It Transmits the size of the sender's buffer (if the buffer is unlimited, the value is 0). If the packet is corrupted, the receiver discards it and the sender resends it after a certain period of time.

##### 4.1.1 Packet structure

| ***Byte*** | 0 | 1 | 2 - EOP |
| ---------- | - | - | ------- |
| ***Information*** | `0x01` | Checksum | Size of the buffer |

#### 4.2 CONNACK

- *code*: `0x02`

Response to CONNECT packet. It is sent if the sender has received a valid connection attempt. It serves as a CONNECT packet for the sender. The sender uses it to send its buffer size. If it is successfully received by the receiver, the connection is successfully established.

##### 4.2.1 Packet structure

| ***Byte*** | 0 | 1 | 2 - EOP |
| ---------- | - | - | ------- |
| ***Information*** | `0x02` | Checksum | Size of the buffer |

#### 4.3 ACCEPTACK

- *code*: `0x03`

A notification that tells an transmitter that the packet has arrived to the receiver and the transmitter can increase the semaphore value.

##### 4.3.1 Packet structure

| ***Byte*** | 0 | 1 |
| ---------- | - | - |
| ***Information*** | `0x03` | Size of accepted packet (in bytes) |

#### 4.4 CRITACK

- *code*: `0x04`

A notification that tells an transmitter that the critical packet has arrived correctly to the receiver and the transmitter can forget that packet and increase the semaphore value.

##### 4.4.1 Packet structure

| ***Byte*** | 0 | 1 |
| ---------- | - | - |
| ***Information*** | `0x04` | ID of accepted critical packet |

#### 4.5 REGTYPE

- *code*: `0x05`

Packet used to register new type on oponent size.

##### 4.5.1 Packet structure

| ***Byte*** | 0 | 1 | 2 | 3 - 4 |
| ---------- | - | - | - | ----- |
| ***Information*** | `0x05` | Checksum | ID | New packet type identifier + severity |

##### 4.5.2 Payload structure

- *bits*: 1**XXXXXXX** 0**X**0000**YY**
- **X** - type identifier of new packet (8b)
- **Y** - severity of new packet (2b)
  - 00 - `NORMAL`
  - 01 - `REPEATED`
  - 10 - `CRITICAL`

#### 4.6 REGACK

- *code*: `0x06`

Packet that serves as a response for successful registration of a new type (REGTYPE).

##### 4.6.1 Packet structure

| ***Byte*** | 0 | 1 | 2 |
| ---------- | - | - | - |
| ***Information*** | `0x06` | Checksum | ID of corresponding REGTYPE packet |

#### 4.7 Type 0

From experience, zero bytes sometimes appear on the serial line, which could cause ambiguities. 

#### 4.8 Type 255

Value 255 (0xFF) is used by serial communication libraries to indicate empty packet buffer, so the packet type will be indistinguishable.

## 5 Communication process flow

### 5.1 Actors

- **A** - communication participant 1
- **B** - communication participant 2
- **DP** - example data packet with type `0x40` and payload `0x05` (one byte)

### 5.2 Process

- *A*: builds CONNECT packet (with its buffer size)
- *A*: sends CONNECT packet
- *B*: accepts *A* CONNECT packet
- *B*: saves *A* buffer size
- *B*: builds CONNACK packet (with its buffer size)
- *B*: sends CONNACK packet to *A*
- *A*: accepts *B* CONNACK packet => connection is established
- *A*: saves *B* buffer size
- ...
- *A*: builds *DP* with given data
- *A*: counts checksum for *DP*
- *A*: sends *DP* to *B*
- *A*: decreases semaphore value by 4
- *B*: receives *DP*
- *B*: validates integrity of *DP* => DP is ok
- *B*: gives *DP* to responsible handler
- *B*: sends ACCEPTACK packet to *A* (with *DP* ID)
- *A*: accepts ACCEPTACK packet
- *A*: increases semaphore value by 4

## 6 Common API

The following public API and its methods must be included in every implementation of the protocol. Their arguments and return types are defined only in general terms and must be always implemented with respect to the implementation platform characteristics.

Implementations may include other data structures and methods in addition to this interface. Especially in case of specific platform needs (e.g. for microcontrollers).

For simplicity, the response to failure is shown here using the concept of exceptions. Each implementation must take advantage of the methods offered by a particular platform (e.g. for Arduino, return codes can be used as return values of methods).

### 6.1 Data types

List of abstract data types that can be used by the following data structures and methods.

| Identifier | Description |
| --- | --- |
| `void` | No return value (only for methods) |
| `bool` | Boolean (`true` or `false`) |
| `i8` | Signed 8 bit integer |
| `i16` |	Signed 16 bit integer |
| `i32` |	Signed 32 bit integer |
| `i64` |	Signed 64 bit integer |
| `u8` | Unsigned 8 bit integer |
| `u16` | Unsigned 16 bit integer |
| `u32` | Unsigned 32 bit integer |
| `u64` | Unsigned 64 bit integer |
| `f32` | 32 bit floating point number |
| `f64` | 64 bit floating point number |
| `char` | Single character |
| `string` | Sequence of characters |
| *`<type>`*`[]` | Array of given *`<type>`* |
| `enum` | Enumeration of concrete values |
| `object` | Instance of an object data type |
| `function` | Function or method |

### 6.2 Public API

The facade of the library. Should be named `Srsc` (the letter case should be chosen with respect to the used platform).

#### 6.2.1 Methods

| Name | Return type | Arguments | Exceptions | Description |
| --- | --- | --- | --- | --- |
| Srsc | - | communicator: `object` | - | Constructor. The `communicator` argument is a reference to an object for handling the communication. |
| begin | `bool` | bufferSize: `u32` | - | A non-blocking attempt to establish a connection to the participant. The *bufferSize* parameter means size of serial buffer of this participant (0 means unlimited). If a successful connection is established, it returns `true`. Otherwise `false`. |
| begin | `void` | bufferSize: `u32`, delay: `u32` | - | A blocking variant of the *begin* method. Attempts to establish a connection are repeated until the connection is successful. The *bufferSize* parameter means size of serial buffer of this participant (0 means unlimited). The *delay* parameter specifies the number of milliseconds between each attempt. |
| registerPacketType | `void` | identifier: `u8`, severity: `enum` | - | Registration of a new packet type. If a packet type with this identifier already exists, it is overridden by this. The `severity` argument can be set to the values: `NORMAL`, `REPEATED` and `CRITICAL`. |
| registerPacketTypeOnBoth | `void` | identifier: `u8`, severity: `enum` | - | Registration of a new packet type on both sides of the channel. If a packet type with this identifier already exists, it is overridden by this. The `severity` argument can be set to the values: `NORMAL`, `REPEATED` and `CRITICAL`. |
| writePacket | `void` | packetType: `u8` | *UnknownPacketTypeException*, *SerialBufferFullException*, *MissingPayloadException* | Attempt to send a payload-less packet to the participant. If the specified packet type is not registered, an *UnknownPacketTypeException* is thrown. If the participant's serial buffer is full (if the packet does not fit in the buffer), a *SerialBufferFullException* is thrown. If the specified packet type requires a payload, a *MissingPayloadException* is thrown. |
| writePacket | `void` | packetType: `u8`, payload: `u8[]` | *UnknownPacketTypeException*, *SerialBufferFullException* | Attempt to send a packet with given payload to the participant. If the specified packet type is not registered, an *UnknownPacketTypeException* is thrown. If the participant's serial buffer is full (if the packet does not fit in the buffer), a *SerialBufferFullException* is thrown. |
| registerOnPacketArrivedCallback | `void` | callback: `function` | - | Registers the function as a callback that is executed when a packet is received (except protocol packets). The function must contain *packetType:* `u8` and *payload:* `u8[]` arguments. |

### 6.3 Optional data structures

The following data structures and methods may be included in the implementation of the protocol to simplify the work with it.

#### 6.3.1 Severity

An enumeration that represents the severity of the packet. Its values are:

- `NORMAL`
- `REPEATED`
- `CRITICAL`

#### 6.3.2 PacketType

A container representing a packet type.

##### 6.3.2.1 Methods

| Name | Return type | Arguments | Exceptions | Description |
| --- | --- | --- | --- | --- |
| PacketType | - | identifier: `u8`, severity: `enum` | - | Constructor. The *severity* is a value from the `Severity` enumeration. |
| getIdentifier | `u8` | - | - | Returns *identifier* value. |
| getSeverity | `enum` | - | - | Returns *severity* value. |

#### 6.3.3 Packet

A container representing a packet.

##### 6.3.3.1 Methods

| Name | Return type | Arguments | Exceptions | Description |
| --- | --- | --- | --- | --- |
| Packet | - | type: `object` | - | Constructor. The *type* argument must be of a `PacketType` type. |
| getType | `object` | - | - | Returns *type* value (instance of `PacketType`). |

#### 6.3.4 DataPacket

A container representing a packet with payload. It is a descendant of the `Packet` structure.

##### 6.3.4.1 Methods

| Name | Return type | Arguments | Exceptions | Description |
| --- | --- | --- | --- | --- |
| DataPacket | - | type: `object`, payload: `u8[]` | - | Constructor with payload represented by *little endian* ordered binary array. The *type* argument must be of a `PacketType` type. |
| getPayload | `u8[]` | - | - | Returns *payload* value. |

## 7 Implementation notes

### 7.1 Connection

After the implementation is initialized, CONNECT packets is sent once (or periodically for blocking initialization). As long as no connection is established, all packets (except CONNECT and CONNACK) are discarded.

If a CONNECT packet is successfully received, a CONNACK is sent back and the sending of CONNECT packets is suspended. If a CONNACK packet is successfully received (after a CONNECT has been sent), the connection is considered established, the sending of CONNECT packets is terminated, and all registered packet types are accepted.

### 7.2 Sending packets

During packet sending (after the connection is established) the semaphore value is decremented by the size of the sent packet. If the semaphore capacity is lower than the packet size, the implementation must report this fact to the rest of the program (e.g. by throwing an exception or returning the corresponding return code).

### 7.3 Receiving packets

When receiving packets (after the connection is established), the packet type is first checked. If the type is registered, the packet checksum is checked. If the check is successful, the packet is passed to the rest of the program. If any of these conditions are not met, the packet is discarded and reading continues with the next packet.

Regardless of the success of the packet acceptance, an ACCEPTACK packet is always sent with the size of the received packet.

### 7.4 Critical packets

The ability to send critical packets does NOT have to be part of every implementation (it is quite memory intensive and could for example cause problems with microcontrollers). On the other hand, the ability to receive them must be always implemented.

When a critical packet is sent, an ID is generated for it and the packet is stored and cannot be removed until confirmation of its acceptance is received. The packet is also resent after a specified time period (which can be set by user).

When a critical packet is successfully received, a CRITACK packet with the received packet ID is sent back. When not, an ACCEPTACK packet with the size of the received packet is sent.

After successful acceptance of the CRITACK packet, the original sender can forget the corresponding critical packet.

### 7.5 Repeated packets

Repeat packets also contain an ID (generated in the same way as for critical packets). The packet is not stored when sent, but instead is sent 3 times (in a row, without interruption).

When a repeat packet is received, it is checked whether it has already been received or not (by its ID). If it has, the packet is discarded. If not, it is accepted and its ID is stored. In both cases, an ACCEPTACK packet with the size of the received packet is sent back.

The implementation must store information about the acceptance/non-acceptance of a packet with specific ID, for the last received repeated packet. When new repeated packed (with new ID) arrives, its ID overrides the old one.

### 7.6 ID

The implementation must include a counter that generates ID values for critical and repeated packets. These values must be sequential, stored separately, and cycled between 0 and 255.

### 7.7 Packet type registration

Registration begins by saving the new type on the sender side. A REGTYPE packet is then sent with the new type information. It is sent periodically until the sender receives a corresponding REGACK packet. REGACK is sent by the receiver if the registration is successful (even if the type is already registered). Otherwise an ACCEPTACK packet with the size of the received packet is send back.

## 8 Current implementations

- [Arduino (C++)](https://codeberg.org/atlas144/srsc-arduino)
- [Java](https://codeberg.org/atlas144/srsc-java)

## 9 Contribution

Contributions are highly welcomed, both for the protocol definition and for its implementations.

## 10 License

The protocol specification is licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/) license.
